![logo](docs/logo.png)

# OSCAR, StarLine open-source car 

OSCAR is an open-source platform for self-driving car development and testing. We aim to achieve [level 4 of driving automation](https://en.wikipedia.org/wiki/Autonomous_car#Levels_of_driving_automation). OSCAR is developed by StarLine, but anyone can contribute, so we are looking forward to your pull-requests.  

## Project status 

We finally bought a car! It's Lexus RX 450H. We decided on this model for several reasons. First of all, it's equipped with powerful batteries that are useful for supplying our systems. Secondly, the vehicle has a CAN bus and working with CAN is our company's developed expertise. Thirdly, it has adaptive cruise control, which is our future ability to easily obtain control over the brakes. And, last but not least, it's gourgeous.    

Currently, we are designing the project's logo, making choise on the central computing unit model, additionally investigating the CAN-bus and also studying car's head unit in order to start developing our own application for it.

* Start. 
* Create [promotion web-site](https://smartcar.starline.ru/) and public portal.
* Select and purchase a vehicle for further development.
* Create a concept.
* Study the chosen vehicle's CAN bus and sensors capabilities. <br />
**===We are here===**  
* Launch a server platform.
* Choose and purchase the platform's CPU. Suitable OS is required.
* Investigate and fine tune CAN bus signals to achieve vihicle's motion control.
* Purchase, install and setup additional sensors (radars, lidars, cameras, satellite navigation, etc.). 
* Develop frameworks for processing data from sensors.
* Develop server--LTE interaction module.
* Develop server software and mobile applications.
* Bench testing.
* Field testing.
* Prepare documentaion.

## Platform architecture 
![platform](docs/platform_en.png)

## Self-driving car
![car](docs/car_en.png)

OSCAR is equipped with
* **Radars** which are necessary for detecting obstacles. The radar signal serves as a starting point for recognizing the object by cameras.  
* **Lidars** are needed to determine the vehicle's position and for obstacle recognition. 
* **RTK GNSS** provides centimeter accuracy geopositioning using GPS+GLONASS and base stations data.   
* **Stereo camera** which is used for object recognition.  
* **Mono camera** identifies obstacles in near-field and recognizes road marking, road signs and traffic lights.  
* **Inertial sensor** allowing to determine the position and direction of a vehicle in cases when data from satellites is not available, e.g. in underground parking or under a bridge.  

### Hardware architecture 
![hardware](docs/hardware_architecture_en.png)

### Software architecture  
![software_architecture](docs/oscar_software_en.png)

* **Perception**: the perception module identifies the world surrounding the autonomous vehicle. There are two important submodules inside perception: obstacle detection and traffic light detection.
* **Prediction**: the prediction module anticipates the future motion trajectories of the perceived obstacles.
* **Routing**: the routing module tells the autonomous vehicle how to reach its destination via a series of lanes or roads.
* **Planning**: the planning module plans the spatio-temporal trajectory for the autonomous vehicle to take.
* **Control**: the control module executes the planned spatiotemporal trajectory by generating control commands such as throttle, brake, and steering.
* **CanBus**: the CanBus is the interface that passes control commands to the vehicle hardware. It also passes chassis information to the software system.
* **HD-Map**: this module is similar to a library. Instead of publishing and subscribing messages, it frequently functions as query engine support to provide ad-hoc structured information regarding the roads.
* **Localization**: the localization module leverages various information sources such as GPS, lidar and IMU to estimate where the autonomous vehicle is located.
* **HMI**: Human Machine Interface is a module for viewing the status of the vehicle, testing other modules and controlling the functioning of the vehicle in real-time.
* **Monitor**: the surveillance system of all the modules in the vehicle including hardware.* **Guardian**: a new safety module that performs the function of an Action Center and intervenes should Monitor detect a failure.


## Server architecture 
![server_architecture](docs/server_architecture_en.png)

* **Communication channels**: an unmanned vehicle will have at least 2 LTE communication channels, through which various data will be transmitted to the StarLine server platform in real time. A smart car will be able to connect via WiFi as well, for maintenance and logs dumping.

* **Telemetry channel (LTE)**: real-time status data and sensor readings transmission to the server. Includes car's location, video stream from cameras, information about driver, CAN bus data and events.

* **Operational control channel (LTE)**: high-priority data transmission which requires immediate response from the car. These are user management commands (start, parking, etc.), route and map data acquisition before the trip (or during the trip), obtaining information about the traffic and weather conditions.

* **Service channel (WiFi)**: wide channel available in the garage for uploading/downloading large data bulks: trip videos from cameras, sensor-fusion data, raw data from the sensors, logs and other data for future analysis.

* **Balancer**: processes connections from vehicle. Responsible for scaling and fault tolerance. Distributes data to one or more data processing services. Based on NGINX.

* **Telemetry data processing service**: highly loaded C++ service for receiving and processing telemetry data of the device and sending data to the message broker cluster.

* **Commands processing service**: C++ service for control commands processing.

* **Message Broker Cluster**: cluster of message brokers based on RabbitMQ.

* **Telemetry data processing service**: processes and stores telemetry data.

* **REST API for users**: API based on OpenApi and the Tornado framework.

* **Simulator API**: API for working with simulator and it's data. 

* **Logs API**: a high-level API for working with logs and dumps.

* **Dump-data storage system**: data storage system.

* **Maps API**: a high-level API for working with maps. The API aggregates the data received from providers of cartographic data and the navigational data from the vehicle. Examples of tasks performed by the API:
  * geocoding
  * reverse geocoding
  * getting the route from the start to end points
  * getting the optimal route from start to end points

* **DB for storage of cartographic data**: not decided yet, maybe some kind of ready-made system.

* **Telemetry data API**: API for storing various types of telemetry data and it's delivery to client applications and services.

* **DB for storing telemetry data**: probably ScyllaDB.

* **Video API**: a service for encoding, decoding and storing video.

## Documentation
At the moment we recommend to refer to this document and the source code. We will provide a more detailed description of the project later on.  

## Contacts 

You can contact us by mail **smartcar@starline.ru**.

And we also have [Twitter](https://twitter.com/starline_oscar) you can subscribe to. 
